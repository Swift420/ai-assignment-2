### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 8891e8b4-689b-4058-92a6-d1ceaf529d9a
using DataStructures

# ╔═╡ fb8fcc10-cab1-11ec-2981-31e37e114fdb
#initialize Closed Set and Open Set
begin
openSet = []
closedSet = []
path = []
	test = []
cols = 3
rows = 	3
end

# ╔═╡ 0e5156f6-acee-4391-beed-355fc1cf93fa
function myshowall(io, x, limit = false) 
  println(io, summary(x), ":")
  Base.print_matrix(IOContext(io, :limit => limit), x)
end

# ╔═╡ 876b00d2-2b96-481e-a8b0-ed89b8998dd2
@enum Action ME MW MU MD CO 

# ╔═╡ f16271fd-e5b2-452f-9154-9de600eb77af
begin

mutable struct Office
	x::Int
	y::Int
	hasAgent::Bool
	#hasParcel::Bool
	noOfParcels::Int
	neighbors::Union{Nothing, Array}
end
end

# ╔═╡ 8f5c7a49-d908-42c8-86c1-352aeec4c68d
struct State	
	office::Matrix{Office}	
end

# ╔═╡ 5f446704-ea36-4451-8f2d-c1251cd80c71
mutable struct Node
	
	
	state::State
	parent::Union{Nothing, Node}
	action::Union{Nothing, Action}
	cost::Int
	
end

# ╔═╡ 95db5a95-ac7e-40ac-af42-214203ca49cf
function action_cost(action)::Int
	cost = 0
	if action == nothing
	cost = 0
	end
	if Action[action] == Action[ME]
		cost = 2
	end	
	if Action[action] == Action[MW]
		cost = 2
	end	
	if Action[action] == Action[MU]
		cost = 1
	end	
	if Action[action] == Action[MD]
		cost = 1
	end	
	if Action[action] == Action[CO]
		cost = 5
	end	
	return cost
end

# ╔═╡ 0ca416aa-ac97-4ab6-aada-9168621b101a
@bind office_per_floorText TextField()

# ╔═╡ f1f072b0-ed2f-4ef3-a815-b577bec9e8d7
@bind num_of_floorsText TextField()

# ╔═╡ 90f39b40-0f1e-43ff-852d-35902be8696c
begin
office_per_floor = parse(Int, office_per_floorText)
num_of_floors = parse(Int,num_of_floorsText)
end

# ╔═╡ 9b3eec1a-2e0c-47bb-ac39-3bcf1e23ecd2
begin
num_Of_Floors = num_of_floors
num_Of_Offices_per_floor = office_per_floor
end

# ╔═╡ 7b28bc2c-82c0-4383-971e-ad19bd78512c
OfficeBuilding = Array{Office}(undef, num_Of_Floors, num_Of_Offices_per_floor)

# ╔═╡ e917966a-9840-4ebf-87a6-59ee74fc49e2
begin
using PlutoUI
	with_terminal() do
myshowall(stdout,OfficeBuilding)
	end
end

# ╔═╡ e65ab550-179f-42b8-82f0-63ce55a833e4
#Give position to nodes 
for i in 1:num_Of_Floors
	for j in 1:num_Of_Offices_per_floor
		OfficeBuilding[i,j] = Office(i,j,false,0,[] )
	end
end

# ╔═╡ 627843e2-ebf6-4428-8c18-a015d4840b27
n1 = Node(State( OfficeBuilding),nothing,nothing, 0)

# ╔═╡ 716f116e-db8b-4298-bff0-9dab0ccf26e2
transitions = Dict{Action, Node}()

# ╔═╡ 54aacbd2-34b9-4a9e-844e-e95f7d380661
function transition_model(node::Node, nodeX, nodeY)
	state = node.state
	for i in 1:state.office[3,end].y
		for j in 1:state.office[end,1].x
	if i >=1 && i <= state.office[3,end].y
		if state.office[nodeX,i].hasAgent
			if state.office[nodeX,i].noOfParcels >= 1
				for k in 1:state.office[nodeX,i].noOfParcels
				
				state.office[nodeX,i].noOfParcels -= 1
				cost = action_cost(CO) 				
				transitions[CO] = Node(State( OfficeBuilding),node,CO, cost)
				
				end
				state.office[nodeX,i].hasAgent = false 
			
			end
			if state.office[nodeX,i].y == state.office[3,end].y
					state.office[nodeX,i].hasAgent = true
				end
		elseif !state.office[nodeX,i].hasAgent && state.office[nodeX,i].noOfParcels >= 1
			#Move to right and change next node to having agent
					cost = action_cost(ME) 	
					transitions[ME] = Node(State( OfficeBuilding),node,ME, cost)
					state.office[nodeX,i].hasAgent = true
			
		end
		
			
		
		end
			
	end
	end
		

	

	
	
	return transitions
	
	
	
end

# ╔═╡ 4d379de5-66dd-4ec6-b5c2-29fddbe81799
function moveRight(office_mat, action, nodeX, nodeY,node)
	if nodeY+1 <= size(office_mat)[2]  
		if !node.state.office[nodeX,nodeY+1].hasAgent #check if y+1 doesnt have an agent
			
			cost = action_cost(action) 
			transitions[action] = Node(State( office_mat),node,action, cost)
				push!(test, transitions[action])
				node.state.office[nodeX,nodeY+1].hasAgent = true
		end
		
		
	end

end

# ╔═╡ 4c276738-6940-4a52-9c91-85f2206477e4
function location_collect(office_mat, action, nodeX, nodeY,node)
	if node.state.office[nodeX,nodeY].hasAgent
				 
					for k in 1:node.state.office[nodeX,nodeY].noOfParcels
				
						node.state.office[nodeX,nodeY].noOfParcels -= 1
						cost = action_cost(action) 				
						transitions[action] = Node(State( office_mat),node,action, cost)
						push!(test, transitions[action])
					end
				node.state.office[nodeX,nodeY].hasAgent = false 
			
				
			
		end
end

# ╔═╡ d499c10f-867c-4204-843f-e8f035dda31c
@bind startYtext TextField()

# ╔═╡ a0ea7e84-64f5-4346-b29e-1f81ad6bd60a
@bind startXtext TextField()

# ╔═╡ 76fc0cc9-ffcd-4e54-b95c-70d7eed38447
begin
startX = parse(Int, startXtext)
startY = parse(Int, startYtext)

	
end

# ╔═╡ 0a545cab-9821-405e-aebb-71ff497f0ae2
#Parcel Locations
begin
	
n1.state.office[1,3].hasAgent = false
n1.state.office[startX,startY].hasAgent = true
n1.state.office[3,1].noOfParcels = 5
n1.state.office[3,2].noOfParcels = 2
n1.state.office[2,2].noOfParcels = 1
n1.state.office[2,3].noOfParcels = 2
n1.state.office[1,1].noOfParcels = 3
n1.state.office[1,3].noOfParcels = 3
	
	
		
	
end

# ╔═╡ 2f71be96-d93a-4fd1-9d07-7c9a430fa52f
begin
	
		empty!(path)
		empty!(test)
		empty!(transitions)
	for j in 1:3
	for i in 1:n1.state.office[1,end].y
		
		#push!(path, transition_model2(n1,2,i))
		#transition_model2(n1,j,i)
		
	end
	end
end

# ╔═╡ 719337b8-7c6c-4c28-935a-c58019f4a981
function is_goal(state::State)
	goal = false
	goal_value = 0
	for i in 1:state.office[end,end].x
		for j in 1:state.office[end,end].y
			goal_value += state.office[i,j].noOfParcels
			
		end
		
	end
	if goal_value == 0
				goal = true
			end
	return goal,goal_value
end

# ╔═╡ c8da4dda-8bde-4705-9e77-181a8965b565
function is_goal_value_x(state::State, row)
	goal = false
	goal_value = 0
	for i in 1:state.office[1,end].y		
		goal_value += state.office[row,i].noOfParcels	
	end
	if goal_value == 0
				goal = true
			end
	return goal,goal_value
end

# ╔═╡ 05894a8b-eb06-4f03-ac92-aef5bb635b19
function transition_model3(node::Node, nodeX, nodeY)
	state = node.state
		
	#If current agent location has parcels then collect
	if !is_goal_value_x(state, nodeX)[1]
		location_collect(OfficeBuilding, CO, nodeX, nodeY,node)
			moveRight(OfficeBuilding, ME, nodeX, nodeY,node)	
	end

	#Move Agent to right and collect
		
	
	println("currently at $(nodeX) , $(nodeY)")
	println(state.office[nodeX,nodeY] == state.office[1,end].y)
	if state.office[nodeX,nodeY] == state.office[1,end] 
		println("this occurs")
		state.office[nodeX,end].hasAgent = true
		
	end

	#Move a floor down, 
	if state.office[1,end].hasAgent && state.office[1,end].noOfParcels == 0
		if !is_goal_value_x(state, nodeX)[1]
			
			for q in 1:3
				
				nodeY -= 1
				if nodeY >= 1  
					
					println("$(q) and wassup1 $(nodeY)")
					if !node.state.office[nodeX,nodeY].hasAgent #check if y+1 doesnt have an agent
			
						cost = action_cost(MW) 
						transitions[MW] = Node(State( OfficeBuilding),node,MW, cost)
						push!(test, transitions[MW])
						node.state.office[nodeX,nodeY].hasAgent = true
					end
					location_collect(OfficeBuilding, CO, nodeX, nodeY,node)
				if state.office[1,nodeY] == state.office[1,1]
					state.office[nodeX,end].hasAgent = false
				end
				end
				
			end
			state.office[nodeX,end].hasAgent = false
			
		elseif is_goal_value_x(state, nodeX)[1]
			println("currently ($(nodeX) $(nodeY)) goal has been reached on1 row")
		end
	end
	if is_goal_value_x(state, nodeX)[1]
			
		
		end
	
	#If agent is at the end 
	
		
	return transitions
	
	
	
end

# ╔═╡ 75211940-1cab-42c9-a6b9-29318f9b956f
function moveUp(office_mat, action, nodeX, nodeY,node)
	if nodeX < 3
	if is_goal_value_x(state, nodeX)[1]
		if state.office[nodeX,nodeY].hasAgent
			#state.office[nodeX,end].hasAgent = false
			cost = action_cost(MD) 
			transitions[MD] = Node(State( OfficeBuilding),node,MD, cost)
			push!(test, transitions[MD])
			#state.office[nodeX,nodeY].hasAgent = true
			state.office[nodeX,nodeY].hasAgent = false
			state.office[nodeX+1,nodeY].hasAgent = true
		end
	end
	end


end

# ╔═╡ 0479d5e7-c252-42fd-8517-7ae4c43377ef
function moveLeft_clear(office_mat, action, state,nodeX,nodeY,node)
	
	if state.office[nodeX,end].hasAgent && state.office[nodeX,end].noOfParcels == 0
		if !is_goal_value_x(state, nodeX)[1]
			
			for q in 1:3
				
				nodeY -= 1
				if nodeY >= 1  
					
					println("$(q) and wassup1 $(nodeY)")
					if !state.office[nodeX,nodeY].hasAgent 
			
						cost = action_cost(action) 
						transitions[action] = Node(State( office_mat),node,action, cost)
						push!(test, transitions[action])
						state.office[nodeX,nodeY].hasAgent = true
					end
					location_collect(office_mat, CO , nodeX, nodeY,node)
				
				end
				
			end
			state.office[nodeX,end].hasAgent = false
			
		elseif is_goal_value_x(state, nodeX)[1]
			println("currently ($(nodeX) $(nodeY)) goal has been reached on1 row")
		end
	end
end

# ╔═╡ 37675b4f-f3ad-47c2-a052-ed58e36385d1
function transition_model2(node::Node, nodeX, nodeY)
	state = node.state

if nodeX < 3
	if is_goal_value_x(state, nodeX)[1]
		if state.office[nodeX,nodeY].hasAgent
			#state.office[nodeX,end].hasAgent = false
			cost = action_cost(MD) 
			transitions[MD] = Node(State( OfficeBuilding),node,MD, cost)
			push!(test, transitions[MD])
			#state.office[nodeX,nodeY].hasAgent = true
			state.office[nodeX,nodeY].hasAgent = false
			state.office[nodeX+1,nodeY].hasAgent = true
		end
	end
end

	
if nodeX < 4
	for i in 1:nodeY
	#If current agent location has parcels then collect
	if !is_goal_value_x(state, nodeX)[1]
		location_collect(OfficeBuilding, CO, nodeX, nodeY,node)
			#Move Agent to right and collect
			moveRight(OfficeBuilding, ME, nodeX, nodeY,node)	
	end



	if state.office[nodeX,nodeY] == state.office[1,end] 
		
		#state.office[nodeX,end].hasAgent = true
		
	end

	#Move a agent left and clear floor, if left has parcels
	moveLeft_clear(OfficeBuilding, MW, state,nodeX,nodeY,node)

	end
	if is_goal_value_x(state, nodeX)[1]
		 
		println("$(nodeX), $(nodeY)")
		println(state.office[nodeX,end].hasAgent)
		
		
		end
end

	

	
	#If agent is at the end 
	
		
	return transitions
	
	
	
end

# ╔═╡ 6b5757ad-08eb-4427-94b5-0d5a9d425aff
function heuristic(node::Node)
	heuristic = 0	
	for i in node.state.office[1,end].x
	for j in node.state.office[end,1].y
		if node.state.office[i,j].noOfParcels == 0 
			heuristic = 3
		end
		end
	end
	
	return heuristic
end

# ╔═╡ 5eb1a25a-b8a2-49ee-9c44-12d964fea790
function a_star_cost(node::Node)
	
	return node.cost
end

# ╔═╡ 32585617-1f6c-457b-81ec-8d0971d478af
begin
empty!(path)
		empty!(test)
		empty!(transitions)
end

# ╔═╡ 1c9468ad-7800-4a86-ab32-60b3c8a34542
function solution1(node::Node, explored::Array)
		empty!(path)
		empty!(test)
		empty!(transitions)
	for j in 1:3
		for i in 1:n1.state.office[1,end].y
		
		push!(path, transition_model2(node,j,i))
		transition_model2(node,j,i)
		
		end
	end
	heuristic(node)
	cost= 0
	for i in 1:length(test)
		cost += test[i].cost
	end
	actions = []
	for node in test
		if !isnothing(node.action)
			push!(actions, node.action)
		end
	end
	
	steps = length(actions)
	return cost," found a path to goal state $(join(actions, "->")) and it took $(steps) steps"
	
end

# ╔═╡ b7482b9e-12f4-4836-abcb-892910e09240
solution1(n1,[])

# ╔═╡ df8b3679-00d6-477c-9f1b-e6c13df6de42
is_goal(n1.state)[1]

# ╔═╡ 9f05cc8a-0a6c-4b5a-bd6c-3345c868f5fc
function failure(message)
	return -1, message
end

# ╔═╡ 1dca6d4c-60d3-45ce-af46-01dffe46ec0f
function a_star_search1(startState::State )
	node = Node(startState,nothing,nothing, 0)
	if is_goal(node.state)[1]
		return solution1(node, [])
	end
	
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier,node, a_star_cost(node) )
	explored = []

	count = 0
	
	y = 1
	x = 1
	while true
		if isempty(frontier)
			return failure("Failure to find solution after exploring all nodes")
		end
		node = dequeue!(frontier)
		for (action,child) in transition_model2(node,x,y)
			if !(child in keys(frontier)) && !(child.state in explored)
				if is_goal(child.state)[1]
					return solution1(child,explored)
				end
				enqueue!(frontier, child, a_star_cost(child))
			end
			x +=1
			y +=1
		end
		if count > 100
			return failure("Timed Out")
		end
	end
end

# ╔═╡ 06cebfe5-5285-4e50-a7c9-7c9504585b1a
#a_star_search1(n1.state)

# ╔═╡ acdbc500-079f-4f36-88d2-6242552d7142
function a_star_search(start::State)
	node = Node(start,nothing,nothing, 0)
	if is_goal(node.state)
		return solution(node, [])
	end
	frontier = PriorityQueue{Node, Int}()
	enqueue!(frontier, node, a_star_cost(node))
	explored = []
	count = 0
	while true
		if isempty(frontier)
			return failure("Failure to find solution after exploring all nodes")
		end
		node = dequeue!(frontier)
		push!(explored,node.state)
		for (action,child) in transition_model2(node)
			if !(child in keys(frontier)) && !(child.state in explored)
				if is_goal(child.state)
					return solution(child,explored)
				end
				enqueue!(frontier, child, a_star_cost(child))
			end
		end
		if count > 100
			return failure("Timed Out")
		end
	end
end

# ╔═╡ 09230294-880d-40f8-97da-69d9e28b2d44
is_goal(n1.state)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═fb8fcc10-cab1-11ec-2981-31e37e114fdb
# ╠═05894a8b-eb06-4f03-ac92-aef5bb635b19
# ╠═54aacbd2-34b9-4a9e-844e-e95f7d380661
# ╠═0e5156f6-acee-4391-beed-355fc1cf93fa
# ╠═876b00d2-2b96-481e-a8b0-ed89b8998dd2
# ╠═8891e8b4-689b-4058-92a6-d1ceaf529d9a
# ╠═f16271fd-e5b2-452f-9154-9de600eb77af
# ╠═8f5c7a49-d908-42c8-86c1-352aeec4c68d
# ╠═5f446704-ea36-4451-8f2d-c1251cd80c71
# ╠═95db5a95-ac7e-40ac-af42-214203ca49cf
# ╠═0ca416aa-ac97-4ab6-aada-9168621b101a
# ╠═f1f072b0-ed2f-4ef3-a815-b577bec9e8d7
# ╠═90f39b40-0f1e-43ff-852d-35902be8696c
# ╠═9b3eec1a-2e0c-47bb-ac39-3bcf1e23ecd2
# ╠═7b28bc2c-82c0-4383-971e-ad19bd78512c
# ╠═e65ab550-179f-42b8-82f0-63ce55a833e4
# ╠═627843e2-ebf6-4428-8c18-a015d4840b27
# ╠═716f116e-db8b-4298-bff0-9dab0ccf26e2
# ╠═4d379de5-66dd-4ec6-b5c2-29fddbe81799
# ╠═4c276738-6940-4a52-9c91-85f2206477e4
# ╠═75211940-1cab-42c9-a6b9-29318f9b956f
# ╠═0479d5e7-c252-42fd-8517-7ae4c43377ef
# ╠═37675b4f-f3ad-47c2-a052-ed58e36385d1
# ╠═d499c10f-867c-4204-843f-e8f035dda31c
# ╠═a0ea7e84-64f5-4346-b29e-1f81ad6bd60a
# ╠═76fc0cc9-ffcd-4e54-b95c-70d7eed38447
# ╠═0a545cab-9821-405e-aebb-71ff497f0ae2
# ╠═2f71be96-d93a-4fd1-9d07-7c9a430fa52f
# ╠═e917966a-9840-4ebf-87a6-59ee74fc49e2
# ╠═719337b8-7c6c-4c28-935a-c58019f4a981
# ╠═c8da4dda-8bde-4705-9e77-181a8965b565
# ╠═6b5757ad-08eb-4427-94b5-0d5a9d425aff
# ╠═5eb1a25a-b8a2-49ee-9c44-12d964fea790
# ╠═32585617-1f6c-457b-81ec-8d0971d478af
# ╠═1c9468ad-7800-4a86-ab32-60b3c8a34542
# ╠═b7482b9e-12f4-4836-abcb-892910e09240
# ╠═df8b3679-00d6-477c-9f1b-e6c13df6de42
# ╠═9f05cc8a-0a6c-4b5a-bd6c-3345c868f5fc
# ╠═1dca6d4c-60d3-45ce-af46-01dffe46ec0f
# ╠═06cebfe5-5285-4e50-a7c9-7c9504585b1a
# ╠═acdbc500-079f-4f36-88d2-6242552d7142
# ╠═09230294-880d-40f8-97da-69d9e28b2d44
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
